/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 9 HW 1 - Search
person class
*/

#ifndef PERSON_H
#define PERSON_H

#define PERSON_FIELDS 5 //The number of fields each person record contains
/*Fields
1. Name  -- Last Name, First Name Middle Initial  -->   Harrison, George A
2. Company --  -->  Exxon Chemical
3 Position/Job Title --  -->    Chemical Engineer
4. Location -- City, State  -->   Baytown, TX
5. Phone # with Area Code  -->   713-555-1234

*/


#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

class Person
{
	string name;
	string company;
	string position;
	string location;
	string phone;

	string fName;
	string lName;
	string mName;

	string city;
	string state;

	void breakDown(string nameIn, string locationIn) //Breaks down name into fName, lName, mName (First name, Last name, Middle name); breaks down location into city and state
	{
		string temp = ""; //temp string to build the entries
		char c;
		int pos = 0; //Position in string
		bool last = 0, first = 0; //Tracks if lastname/firstname have been extracted yet

		while (pos < int(nameIn.length())) //Breaks down the name string into last, first, and middle
		{
			if (nameIn.at(pos) == ',') // Found a comma, that's the last name
			{
				lName = temp; //Last name's built in the temp string, pull it out
				temp = ""; //clear the temp string in preperation for grabbing the first name
				pos++; //Move to the character after the comma
				if (nameIn.at(pos) == ' ') //Check if it's a space, move one more if so
				{
					pos++;
				}
				last = 1;
			}
			else if (nameIn.at(pos) == ' ' && last == 1) // Found a space after pulling out the last name, first name's built
			{
				fName = temp; //first name's built in the temp string, pull it out
				temp = ""; //clear the temp string in preperation for grabbing the middle initial
				pos++; //Move to the character after the space
				first = 1;

				c = nameIn.at(pos); //Pulls a character from the string
				string str(1, c); // Converts the character to string
				temp.append(str); // Adds it to the word we're building, congrats that's the middle initial
				mName = temp;
			}
			else
			{
				c = nameIn.at(pos); //Pulls a character from the string
				string str(1, c); // Converts the character to string
				temp.append(str); // Adds it to the word we're building
				pos++; // Move to next character
			}
		}

		pos = 0; //Reset position marker
		temp = ""; //clear the temp string 

		while (pos < int(locationIn.length())) //Breaks down the Location string into city and state
		{
			if (locationIn.at(pos) == ',') // Found a comma, built the city
			{
				city = temp; //City's built in the temp string, pull it out
				temp = ""; //clear the temp string in preperation for grabbing the state
				pos++; //Move to the character after the comma
				if (locationIn.at(pos) == ' ') //Check if it's a space, move one more if so
				{
					pos++;
				}

				c = locationIn.at(pos); //Pulls a character from the string
				string st1(1, c); // Converts the character to string
				temp.append(st1); // Adds it to the state we're building
				pos++; // Move to next character

				c = locationIn.at(pos); //Pulls a character from the string
				string st2(1, c); // Converts the character to string
				temp.append(st2); // Adds it to the state we're building
				state = temp; //Pulls the completed state extraction from the temp string
				//We're done
			}

			else
			{
				c = locationIn.at(pos); //Pulls a character from the string
				string str(1, c); // Converts the character to string
				temp.append(str); // Adds it to the word we're building
				pos++; // Move to next character
			}
		}
	}


public:

	// Constructors
	Person(string nameIn, string companyIn, string positionIn, string locationIn, string phoneIn) //Constructor, with initial data supplied
	{
		name = nameIn;
		company = companyIn;
		position = positionIn;
		location = locationIn;
		phone = phoneIn;
		breakDown(name, location);
	}
	Person(string info[]) //Constructor with values, accepts an 5 slot string array //PERSON_FIELDS
	{
		name = info[0];
		company = info[1];
		position = info[2];
		location = info[3];
		phone = info[4];
		breakDown(name, location);
	}
	Person() //Constructor, without initial data
	{

		name = "Unknown";
		company = "Unknown";
		position = "Unknown";
		location = "Unknown";
		phone = "Unknown";

		fName = "Unknown";
		lName = "Unknown";
		mName = "Unknown";

		city = "Unknown";
		state = "UN";
	}

	void getAll(string info[]) //Fills the given array with the person's information
	{
		info[0] = name;
		info[1] = company;
		info[2] = position;
		info[3] = location;
		info[4] = phone;
	}
	void setAll(string info[]) //Overrites the persons's information with that from an array
	{
		name = info[0];
		company = info[1];
		position = info[2];
		location = info[3];
		phone = info[4];
	}

	string getName()
	{
		return name;
	}
	void setName(string nameIn)
	{
		name = nameIn;
	}

	string getCompany()
	{
		return company;
	}
	void setCompany(string companyIn)
	{
		company = companyIn;
	}

	string getPosition()
	{
		return position;
	}
	void setPosition(string positionIn)
	{
		position = positionIn;
	}

	string getLocation()
	{
		return location;
	}
	void setLocation(string locationIn)
	{
		location = locationIn;
	}

	string getPhone()
	{
		return phone;
	}
	void setPhone(string phoneIn)
	{
		phone = phoneIn;
	}

	string getfName()
	{
		return fName;
	}
	string getlName()
	{
		return lName;
	}
	string getmName()
	{
		return mName;
	}

	string getCity()
	{
		return city;
	}
	string getState()
	{
		return state;
	}

	void printShort() //Prints the name and position of a person
	{
		cout << "NAME: " << getName() << endl;
		cout << "POSITION: " << getPosition() << endl;
		cout << endl;
	}

	void printAll() //Prints all the details of a person
	{
		cout << "NAME: " << getName() << endl;
		cout << "COMPANY: " << getCompany() << endl;
		cout << "TITLE: " << getPosition() << endl;
		cout << "LOCATION: " << getLocation() << endl;
		cout << "PHONE NUMBER: " << getPhone() << endl;

		cout << endl;
	}
};

#endif