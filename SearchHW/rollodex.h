/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 9 HW 1 - Search
person case
*/


#ifndef ROLLODEX_H
#define ROLLODEX_H

#include "person.h"
#include "ldeque.h"
#include <iomanip>
#include <stdio.h>
#include <string>

using namespace std;


class Rollodex : public LDeck<Person>
{
	Person* helpGetPerson(int personNumber) //returns a pointer to a specific person, as specified by the person number
	{
		personNumber = personNumber - 1; //Adjusts the personNumber from a list starting at 1 to one starting at 0

		if (personNumber < 0 || personNumber > length()) //Checks if the personNumber given is 0, negative, or out of bounds
		{
			cout << "There is no person #";
			if (personNumber < 10)
			{
				cout << setfill('0') << setw(2) << personNumber;
			}
			else
			{
				cout << personNumber;
			}
			cout << "." << endl;
			abort();
		}

		Person temp;
		Person* outPerson;
		Link<Person>* tempLink;
		LDeck<Person> tempdeck; // Temp deck for books we pull while getting to the person we want

		for (int x = 0; x < personNumber; x++) //Pulls the books in the way into a holding deck
		{
			temp = dequeue(); //Pulls a person out
			tempdeck.enqueue(temp); //Puts it in a holding deck
		}

		tempLink = frontLink(); //Gets a pointer to the front link
		outPerson = &(tempLink->element); //Gets a pointer to the person

		while (tempdeck.length() > 0) //stuffs the books from the holding deck back into the bookcase
		{
			temp = tempdeck.dedeck();
			endeck(temp);
		}

		return outPerson;

		//Could rework to avoid the tempdecks and dequeuing via setting outbook equal to the front pointer and then looping outPerson=outPerson->next until it hits personNumber but this works
	}

	string extractor(string lineIn, int& pos) //Breaks up the giant line into chunks seperated by commas
	{
		string temp = ""; //temp string to build the entry
		char c;
		while (pos < int(lineIn.length()))
		{
			if ((lineIn.at(pos) == '|' && lineIn.at(pos+1) == '|') || pos+1 == int(lineIn.length())) // Found a "||", that's one field, or we're at the end of the string
			{
				if (pos + 1 == int(lineIn.length())) //End of the string
				{
					c = lineIn.at(pos); //Pulls a character from the string
					string str(1, c); // Converts the character to string
					temp.append(str); // Adds it to the word we're building
					return temp; //Returns the word we've built
				}
				else //Found a ||
				{
					pos++; //Skip the seperator
					pos++;
					return temp;
				}
			}

			else
			{
				c = lineIn.at(pos); //Pulls a character from the string
				string str(1, c); // Converts the character to string
				temp.append(str); // Adds it to the word we're building
				pos++; // Move to next character
			}
		}
		return temp; //Reached the end
	}

public:

	void fileIn(string filename) //Reads a set of persons from a correctly formatted text file with the given filename, loads the rollodex with it
	{
		string line;
		ifstream myfile(filename); //Open the word list
		if (myfile.is_open())
		{
			cout << "Loading employees from \"" << filename << "\"." << endl;

			while (getline(myfile, line)) // Grabs each line from the text file, stores them in the info array
			{
				string info[PERSON_FIELDS]; //Array to hold the person information before it is inserted into the person object
				int position = 0; //Holds where we left off last time we ran extractor
				for (int x = 0; x < PERSON_FIELDS; x++) //Operates PERSON_FIELDS (5) times, using extractor to fill each slot in the array
				{

					info[x] = extractor(line, position);

					if (x == 4)//Just put in the phone number, build the person
					{

					}
				}
				Person temp(info);//Take a completed individual's info and stuff it into a person
				enqueue(temp); //Enqueue the person in the rollodex, move on to the next
			}
			cout << "Finished loading." << endl;
			myfile.close(); // Closes the file
		}

		else
		{
			cout << "Unable to open file.";
			abort(); //Can't open the file, print an error and kill program
		}
	}

	void list() //Lists the title and author of every person in the bookcase
	{
		Person* temp; //Pointer to the person we're printing
		int x = 1;
		for (int x = 1; x - 1 < length(); x++)
		{
			if (length() == 1) //Last person
			{
				temp = helpGetPerson(x); //Gets a pointer to the [x]th person
				cout << "Person #";
				if (x < 10) //Formatting stuff
				{
					cout << setfill('0') << setw(2) << x;
				}
				else
				{
					cout << x;
				}
				cout << ":" << endl;

				temp->printShort();
				cout << "End of list." << endl;
			}
			else
			{
				temp = helpGetPerson(x); //Gets a pointer to the [x]th person
				cout << "Person #";
				if (x < 10) //Formatting stuff
				{
					cout << setfill('0') << setw(2) << x;
				}
				else
				{
					cout << x;
				}
				cout << ":" << endl;
				temp->printShort();
			}
		}

	}

	void view(int personNumber) //Prints all the details of a given person, indicated by its position in the deck
	{

		Person* temp;
		temp = helpGetPerson(personNumber);

		temp->printAll(); //Prints its info
	}

	void add(string info[]) //Adds a person to the bookcase, info generated via a string array
	{
		/*Fields
		1. Name  -- Last Name, First Name Middle Initial  -->   Harrison, George A
		2. Company --  -->  Exxon Chemical
		3 Position/Job Title --  -->    Chemical Engineer
		4. Location -- City, State  -->   Baytown, TX
		5. Phone # with Area Code  -->   713-555-1234

		*/
		Person temp; // Temp person to hold our information
		temp.setAll(info); //Fills in the temp person
		enqueue(temp); //Adds the filled in temp person to the bookcase
	}

	void kill(int personNumber) //Removes a given person, indicated by its position in the deck
	{
		personNumber = personNumber - 1; //Adjusts the personNumber from a list starting at 1 to one starting at 0

		if (personNumber < 0 || personNumber > length()) //Checks if the personNumber given is 0, negative, or out of bounds
		{
			cout << "There is no person #";
			if (personNumber < 10)
			{
				cout << setfill('0') << setw(2) << personNumber;
			}
			else
			{
				cout << personNumber;
			}
			cout << "." << endl;
			return;
		}

		Person temp;
		LDeck<Person> tempdeck; // Temp deck for books we pull while getting to the person we want

		for (int x = 0; x < personNumber; x++) //Pulls the books in the way into a holding deck
		{
			temp = dequeue(); //Pulls a person out
			tempdeck.enqueue(temp); //Puts it in a holding deck
		}

		temp = dequeue(); //Removes the person we want 


		while (tempdeck.length() > 0) //stuffs the books from the holding deck back into the bookcase
		{
			temp = tempdeck.dedeck();
			endeck(temp);
		}
	}

	void getInfo(int personNumber, string info[]) //Gets all of the information of a specific person and returns it in a given array
	{
		Person* temp; //Pointer to the person we're scraping info from
		temp = helpGetPerson(personNumber); //Sets the pointer to the right person
		temp->getAll(info); //Fills the info array
	}


	void updatePerson(int personNumber, string info[]) //Updates all the information of a specified person with the information from a given array
	{

		Person* temp; //Pointer to the person we're updating
		temp = helpGetPerson(personNumber); //Sets the pointer to the right person
		temp->setAll(info); //Fills the info array
	}

	/*Not fully updated for rollodex, still bookcase-only
	void save(string filename) //Saves the current rollodex to the specified filename
	{
		ifstream myfile(filename); //Open the word list
		if (myfile.is_open())
		{
			remove((filename.c_str()));//Delete the file
			myfile.close(); // Closes the file. Do I need to do this if I'm deleting it?
		}
		else
		{
			cout << "Can't delete " << filename << endl;
			//Can't open the file, don't think this should cause a problem
		}

		cout << "Saving rollodex to \"" << (filename.c_str()) << "\"." << endl;
		Person* temp; //Pointer to each person
		string info[PERSON_FIELDS]; //Array to hold the person information before it is saved to the file

		ofstream outfile((filename.c_str())); //Creates a text file we're going to save everything to 

		if (outfile.is_open())
		{
			for (int x = 1; x - 1 < length(); x++) //Takes each person in the rollodex and saves it
			{

				temp = helpGetPerson(x); //Sets the pointer to the right person
				temp->getAll(info); //Fills the info array

				for (int y = 0; y < PERSON_FIELDS; y++) //Operates on each field in the info array
				{
					if (y == 7) //Hit the last field
					{
						outfile << info[y] << endl << endl;
					}
					else
					{
						outfile << info[y] << endl;
					}
				}
			}
			outfile.close(); //Closes the finished file
		}

		else cout << "Unable to open file" << endl;

		cout << "Finished saving." << endl;
	}
	*/
	void searchSequential(string name) //Accepts a last name and searches the rollodex for the first match. If it finds a match, 
										//it runs said person's printAll() on it to print its information, otherwise prints an error
	{
		bool found = 0;
		Person* temp = nullptr;
		for (int x = 1; (found == 0 && x < length()); x++) //Runs until it either finds a match or hits the end of the 'dex
		{
			temp = helpGetPerson(x); //Gets a pointer to the person [x]th in the 'dex
			if (name.compare((temp->getlName())) == 0) //Found a match on the name
			{
				found = 1;
			}
		}

		if (found == 1)
		{
			cout << "\"" << name << "\" found." << endl;
			temp->printAll();
		}
		else
		{
			cout <<"\"" << name << "\" Cannot be found." << endl;
		}

	}


	void sortInsertion() //Sorts by phone numbers
	{
		if (length() == 0)
		{
			cout << "Deque is empty. Invalid sort." << endl;
			abort();
		}
		else
		{
			int swaps = 0, compares = 0;
			Link<Person>* lastSorted = getFront()->next; //points to the end of the sorted section, here the first link in the deque
			cout << "Insertion Sorting..." << endl;

			while (getRear() != lastSorted) //While the end of the sorted section isn't the end of the deck
			{
				Link<Person>* tempLook = lastSorted; //Points to the last sorted link
				Link<Person>* workingLink = lastSorted->next; //points to link we're working with, here the second link in the deck
				Person personLook, personWorking; //Temp people to hold values
				string look, work;
				bool swapping = true;
				while (swapping == true)
				{
					personLook = tempLook->element;
					personWorking = workingLink->element;

					if (personWorking.getPhone() > personLook.getPhone()) //Is the element's phone number in the card we're working on greater than the one we're currently looking at?
					{
						//Yes, insert the link we're working with after the link we're looking at, increment swaps counter
						if (tempLook->next == workingLink) //WorkingLink is immediately after the link we're looking at
						{
							//Don't need to move the link we're working with
							//just need to increment the last sorted marker so that it is pointing to the link we're working with, then work on the next link
							lastSorted = lastSorted->next;//increments sort marker
							swapping = false;//Done working with this link, move to the next
						}
						else
						{
							insertAfter(workingLink, tempLook);
							//lastSorted = lastSorted->next; //increment sort marker //Automatically done by inserting directly rather than using swaps, video advice was array-based
							swaps++;
							swapping = false; //Done swapping for this link, start moving the next one
						}
					}
					else
					{
						//No, move where we're looking one to the left, unless it's about to hit the front ghost link, in which case we place it immediately after 
						if (tempLook->prev == getFront())
						{
							insertAfter(workingLink, tempLook->prev);
							swaps++;
							swapping = false; //Done swapping for this link, start moving the next one
						}
						else
						{
							tempLook = tempLook->prev;

						}
					}
					compares++; //Increments compares
				}
			}
			cout << "Finished insertion sorting. Compares: " << compares << " Swaps: " << swaps << endl << endl;
		}
	}


	void searchBinary(string number) //Accepts a phone number and searches the rollodex for the first match. If it finds a match, 
									   //it runs said person's printAll() on it to print its information, otherwise prints an error
	{
		bool found = 0;
		Person* temp = nullptr;
		int left = 0, right = length(); //Left and right most positions of the section we're checking

		while (left <= right && found == 0) //Operates until either it's gone through everything or found a match
		{
			int middle = left + (right - left) / 2; //Gets the middle of the section we're checking

			temp = helpGetPerson(middle); //Gets a pointer to the middle person in the section of the 'dex we're checking 
			if (number.compare((temp->getPhone())) == 0) //Found a match on the number
			{
				found = 1;
			}
			else //Didn't find a match
			{
				if (number > (temp->getPhone())) //The number we're looking for is greater than what we looked at, ignore left half
				{
					left = middle + 1; //Set left bound to immediately after middle
				}
				else //The number we're looking for is smaller than what we looked at, ignore right half
				{
					right = middle - 1; //sets right bound to immediately after middle
				}
			}

		}


		if (found == 1) //found it
		{
			cout << "\"" << number << "\" found." << endl;
			temp->printAll();
		}
		else //didn't find it
		{
			cout << "\"" << number << "\" Cannot be found." << endl;
		}

	}

};
#endif