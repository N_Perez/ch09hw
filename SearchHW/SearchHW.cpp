/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 9 HW 1 - Search
*/



#include <iostream>
#include "rollodex.h"
#include <iomanip>

using namespace std;

int main()
{
	Rollodex fillo;
	fillo.fileIn("records.txt");
	
	fillo.searchSequential("Kirissame");
	fillo.searchSequential("Kirisame");

	fillo.searchSequential("Moroboshi");
	fillo.searchSequential("Sanchez");

	fillo.sortInsertion();

	fillo.searchBinary("486-555-9804");	
	fillo.searchBinary("867-5309");

	system("pause");
	return 0;
}